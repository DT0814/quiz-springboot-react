import App from "./App";
import React from "react";
import * as ReactDom from "react-dom";


ReactDom.render(<App />, document.getElementById("root"));

