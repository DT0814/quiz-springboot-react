import React from 'react'

export default class GuessButton extends React.Component {
  constructor(props) {
    super(props);

  }

  playGameClick() {
    if (this.props.locationStr.length === 0) {
      alert('please create game');
      return;
    }
    //this.props.current发送请求拿到结果，调用父组件的方法
    this.props.changeRecord('1A2B');
  }

  render() {
    return (
      <button onClick={this.playGameClick.bind(this)} className='button-class'>Guess</button>
    )
  }
}
