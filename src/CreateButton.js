import React from 'react'

export default class CreateButton extends React.Component {

  handlerClick() {
    this.props.changeLoaction('/api/games/1')
  }

  render() {
    return (
      <button onClick={this.handlerClick.bind(this)} className='button-class'>New Game</button>
    )
  }
}
